export const storeProducts=[
    {
        id:1,
        title:"Google Pixel - Black",
        img:"img/car.jpg",
        price:10,
        company:"Google",
        info:
        "  Lorem ipsum dolor sit amet, consectetur adipisicing elit." +
            " Commodi exercitationem explicabo ipsum labore laborum" +
            " magni maxime molestiae quas repellendus vel.\n",
        inCart:false,
        count:0,
        total:0
    },
    {
        id:2,
        title:"Google Pixel - Black",
        img:"img/car.jpg",
        price:15,
        company:"Samsung",
        info:
            "  Lorem ipsum dolor sit amet, consectetur adipisicing elit." +
            " Commodi exercitationem explicabo ipsum labore laborum" +
            " magni maxime molestiae quas repellendus vel.\n",
        inCart:false,
        count:0,
        total:0
    },
    {
        id:3,
        title:"Google Pixel - Black",
        img:"img/car.jpg",
        price:10,
        company:"Amazon",
        info:
            "  Lorem ipsum dolor sit amet, consectetur adipisicing elit." +
            " Commodi exercitationem explicabo ipsum labore laborum" +
            " magni maxime molestiae quas repellendus vel.\n",
        inCart:false,
        count:0,
        total:0
    },
    {
        id:4,
        title:"Google Pixel - Black",
        img:"img/car.jpg",
        price:20,
        company:"Yandex",
        info:
            "  Lorem ipsum dolor sit amet, consectetur adipisicing elit." +
            " Commodi exercitationem explicabo ipsum labore laborum" +
            " magni maxime molestiae quas repellendus vel.\n",
        inCart:false,
        count:0,
        total:0
    },
    {
        id:5,
        title:"Google Pixel - Black",
        img:"img/car.jpg",
        price:5,
        company:"Google",
        info:
            "  Lorem ipsum dolor sit amet, consectetur adipisicing elit." +
            " Commodi exercitationem explicabo ipsum labore laborum" +
            " magni maxime molestiae quas repellendus vel.\n",
        inCart:false,
        count:0,
        total:0
    },
    {
        id:6,
        title:"Google Pixel - Black",
        img:"img/car.jpg",
        price:25,
        company:"Samsung",
        info:
            "  Lorem ipsum dolor sit amet, consectetur adipisicing elit." +
            " Commodi exercitationem explicabo ipsum labore laborum" +
            " magni maxime molestiae quas repellendus vel.\n",
        inCart:false,
        count:0,
        total:0
    },
    {
        id:7,
        title:"Google Pixel - Black",
        img:"img/car.jpg",
        price:30,
        company:"Amazon",
        info:
            "  Lorem ipsum dolor sit amet, consectetur adipisicing elit." +
            " Commodi exercitationem explicabo ipsum labore laborum" +
            " magni maxime molestiae quas repellendus vel.\n",
        inCart:false,
        count:0,
        total:0
    },
    {
        id:8,
        title:"Google Pixel - Black",
        img:"img/car.jpg",
        price:20,
        company:"Alibaba",
        info:
            "  Lorem ipsum dolor sit amet, consectetur adipisicing elit." +
            " Commodi exercitationem explicabo ipsum labore laborum" +
            " magni maxime molestiae quas repellendus vel.\n",
        inCart:false,
        count:0,
        total:0
    }
];
export const detailProduct={
    id:1,
    title:"Google Pixel - Black",
    img:"img/car.jpg",
    price:20,
    company:"Samsung",
    info:
    "  Lorem ipsum dolor sit amet, consectetur adipisicing elit." +
    " Commodi exercitationem explicabo ipsum labore laborum" +
    " magni maxime molestiae quas repellendus vel.\n",
        inCart:false,
    count:0,
    total:0
};