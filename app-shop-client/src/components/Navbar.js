import React, {Component} from 'react';
import {Link} from "react-router-dom";
import {FcIphone} from "react-icons/fc";
import {FaCartPlus} from "react-icons/fa";
import styled from 'styled-components';
import {ButtonContainer} from "./Button";
class Navbar extends Component {
    render() {
        return (
            <NavWrapper className="navbar navbar-expand-sm navbar-dark px-sm-5">
                <Link to="/">
                    <FcIphone className="navbar-brand" style={{fontSize:50}}/>
                </Link>
                <ul className="navbar-nav align-items-center">
                    <li className="nav-item ml-5">
                        <Link to="/" className="nav-link">products</Link>
                    </li>
                </ul>
                <Link to="/cart" className="ml-auto">
                    <ButtonContainer>
                        <span className="mr-2"><FaCartPlus/></span>
                        My cart
                    </ButtonContainer>
                </Link>
            </NavWrapper>
        );
    }
}

const NavWrapper=styled.nav`
background:var(--mainBlue);
.nav-link{
color:var(--mainWhite)!important;
font-size:1.3rem;
text-transform:capitalize;
}
`

export default Navbar;