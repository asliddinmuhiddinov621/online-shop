package com.mlc.appshopserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppShopServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppShopServerApplication.class, args);
	}

}
